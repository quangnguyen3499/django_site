from django.urls import path
from . import views

app_name = 'polls'
urlpatterns = [
  path('', views.IndexView.as_view(), name='index'),
  path('<int:pk>/', views.DetailView.as_view(), name='detail'),
  path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
  path('<int:question_id>/vote/', views.vote, name='vote'),
  path('register/', views.Register.as_view(), name='Register'),
  path('login/', views.CustomLogin.as_view(), name='login'),
  path('logout/', views.CustomLogout.as_view(), name='logout')
]