from rest_framework import serializers

from .models import Question, MyUser

class QuestionSerializer(serializers.HyperlinkedModelSerializer):
  class Meta:
    model = Question
    fields = ('question_text', 'pub_date')
    
class UserSerializer(serializers.ModelSerializer):  
  class Meta:
    model = MyUser
    fields = ('id', 'email', 'is_active', 'is_admin')