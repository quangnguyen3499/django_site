from django.db import models
from django.utils import timezone
import datetime
from django.contrib import admin
from django.contrib.auth.models import (
  BaseUserManager, AbstractBaseUser
)

class Question(models.Model):
  question_text = models.CharField(max_length=200)
  pub_date = models.DateTimeField('date published')

  def __str__(self):
    return self.question_text
  
  def has_short_question(self):
    return len(self.question_text) < 2
  @admin.display(
    boolean=True,
    ordering='pub_date',
    description='Published recently?',
  )

  def was_published_recently(self):
    now = timezone.now()
    return now >= self.pub_date >= timezone.now() - datetime.timedelta(days=1)

class Choice(models.Model):
  question = models.ForeignKey(Question, on_delete=models.CASCADE)
  choice_text = models.CharField(max_length=200)
  votes = models.IntegerField(default=0)

  def __str__(self):
    return self.choice_text
  
class MyUserManager(BaseUserManager):
  def create_user(self, email, password=None):
    if not email:
      raise ValueError('Users must have an email address')

    user = self.model(
      email=self.normalize_email(email)
    )

    user.set_password(password)
    user.save(using=self._db)
    return user

  def create_superuser(self, email, password=None):
    user = self.create_user(
      email,
      password=password
    )
    user.is_admin = True
    user.save(using=self._db)
    return user

class MyUser(AbstractBaseUser):
  email = models.EmailField(
    verbose_name='email address',
    max_length=255,
    unique=True,
  )
  is_active = models.BooleanField(default=True)
  is_admin = models.BooleanField(default=False)

  objects = MyUserManager()

  USERNAME_FIELD = 'email'

  def __str__(self):
    return self.email

  @property
  def is_staff(self):
    return self.is_admin