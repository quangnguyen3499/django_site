from django.http.response import HttpResponseRedirect
from rest_framework.views import APIView
from polls.serializers import UserSerializer
from .models import MyUser, MyUserManager, Question, Choice
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic
from rest_framework.response import Response
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
import json

class IndexView(generic.ListView):
  template_name = 'polls/index.html'
  context_object_name = 'latest_question_list'

  def get_queryset(self):
    # return last 5 question
    # return Question.objects.filter(
    #   pub_date__lte = timezone.now()
    # ).order_by('-pub_date')[:5]
    # return Question.objects.filter(pub_date__lte=timezone.now())
    return Question.objects.all()

class DetailView(generic.DetailView):
  model = Question
  template_name = 'polls/detail.html'

class ResultsView(generic.DetailView):
  model = Question
  template_name = 'polls/results.html'
  
def vote(request, question_id):
  question = get_object_or_404(Question, pk=question_id)
  try:
    selected_choice = question.choice_set.get(pk=request.POST['choice'])
  except (KeyError, Choice.DoesNotExist):
    return render(request, 'polls/detail.html', {
      'question': question,
      'error_message': "You didn't select a choice",
    })
  else:
    selected_choice.votes += 1
    selected_choice.save()
  return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))

class Register(APIView):
  def post(self, request):
    user = MyUser.objects.create_user(
      email=request.data['username'],
      password=request.data['password']
    )
    response_data = {
      'user': UserSerializer(user).data
    }
    return Response(response_data)
  
class CustomLogin(ObtainAuthToken):
  def post(self, request, *args, **kwargs):
    user = MyUser.objects.get(email=request.data['username'])
    serializer_user = UserSerializer(user).data
    token, created = Token.objects.get_or_create(user=user)
    response_data = {
      'user': serializer_user,
      'token': token.key
    }
    return Response(response_data)
  
class CustomLogout(APIView):
  def get(self, request):
    Token.objects.get(user=request.user).delete()
    response_data = {
      'status': 204,
      'message': "User logged out"
    }
    return Response(response_data)