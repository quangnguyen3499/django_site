from django.apps import AppConfig

class CrawlerDemoConfig(AppConfig):
  default_auto_field = 'django.db.models.BigAutoField'
  name = 'crawler_demo'
