from django.urls import path
from . import views

app_name = 'crawler_demo'
urlpatterns = [
  path('', views.index, name='index'),
]